#!/usr/bin/env bash

if [ "$RACK_ENV" = 'development' ]; then
    username=$MYSQL_USER
    password=$MYSQL_PASSWORD
    url='sonar-database:3306'
fi

echo "DB username: $username"
echo "DB password: $password"
echo "DB url: $url"

sed -i "s/#sonar.jdbc.username=/sonar.jdbc.username=$username/" $SONARQUBE_HOME/conf/sonar.properties
sed -i "s/#sonar.jdbc.password=/sonar.jdbc.password=$password/" $SONARQUBE_HOME/conf/sonar.properties
sed -i 's/sonar.jdbc.url=jdbc:h2/#sonar.jdbc.url=jdbc:h2/g'  $SONARQUBE_HOME/conf/sonar.properties
sed -i "s/#sonar.jdbc.url=jdbc:mysql:\/\/localhost:3306/sonar.jdbc.url=jdbc:mysql:\/\/$url/" $SONARQUBE_HOME/conf/sonar.properties

$SONARQUBE_HOME/bin/linux-x86-64/sonar.sh start
/scripts/activate_rules.py &

tail -f $SONARQUBE_HOME/logs/sonar.log