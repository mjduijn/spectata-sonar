#!/usr/bin/env python

import urllib, urllib2, base64, json, time

# time.sleep(20)
# print "Activating pitest rules at: %s" % time.ctime()

sonar_url = "http://localhost:9000"

#Retrieve all quality profiles
# request = urllib2.Request(sonar_url + '/api/qualityprofiles/search')
# base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')
# request.add_header("Authorization", "Basic %s" % base64string)
# try:
#     profiles = json.load(urllib2.urlopen(request))["profiles"]
# except urllib2.HTTPError as e:
#     print e.code
#     print e.read()
#     exit(1)
#
#
# #Get all Pitest rules
# params = urllib.urlencode({'repositories':  'pitest'})
# request = urllib2.Request(sonar_url + '/api/rules/search', params)
# request.add_header("Authorization", "Basic %s" % base64string)
# try:
#     rules = json.load(urllib2.urlopen(request).fp)["rules"]
# except urllib2.HTTPError as e:
#     print e.code
#     print e.read()
#     exit(1)
#
#
# #Activate the pitest rules in all profiles
# for profile in profiles:
#     for rule in rules:
#         params = urllib.urlencode({'profile_key':  profile['key'], 'rule_key': rule['key']})
#         request = urllib2.Request(sonar_url + '/api/qualityprofiles/activate_rule', params)
#         request.add_header("Authorization", "Basic %s" % base64string)
#         try:
#             urllib2.urlopen(request)
#         except urllib2.HTTPError as e:
#             print e.code
#             print e.read()
#             exit(1)

#Disable all rules
time.sleep(30)
print "Disabling all rules at: %s" % time.ctime()

request = urllib2.Request(sonar_url + '/api/qualityprofiles/search')
base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')
request.add_header("Authorization", "Basic %s" % base64string)
try:
    profiles = json.load(urllib2.urlopen(request))["profiles"]
except urllib2.HTTPError as e:
    print e.code
    print e.read()
    exit(1)


#Get all rules
request = urllib2.Request(sonar_url + '/api/rules/search')
request.add_header("Authorization", "Basic %s" % base64string)
try:
    rules = json.load(urllib2.urlopen(request).fp)["rules"]
except urllib2.HTTPError as e:
    print e.code
    print e.read()
    exit(1)


#Dectivate rules in all profiles
for profile in profiles:
    for rule in rules:
        params = urllib.urlencode({'profile_key':  profile['key'], 'rule_key': rule['key']})
        request = urllib2.Request(sonar_url + '/api/qualityprofiles/deactivate_rule', params)
        request.add_header("Authorization", "Basic %s" % base64string)
        try:
            urllib2.urlopen(request)
        except urllib2.HTTPError as e:
            print e.code
            print e.read()
            exit(1)